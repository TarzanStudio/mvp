package me.alide.mvp.base.mvp;

/**
 * Created by wutairui on 16/3/16.
 */
public interface MvpView {

    void onFailure(Throwable e);
}
