package me.alide.mvp.base.mvp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import me.alide.mvp.R;
import me.alide.mvp.data.DataManager;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by wutairui on 16/3/16.
 */
public class BasePresenter<T extends MvpView> implements Presenter<T>{

    private T mMvpView;
    public CompositeSubscription mCompositeSubscription;
    public DataManager mDataManager;
    public Context context;

    public BasePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(T mvpView) {
        this.mMvpView = mvpView;
        this.mCompositeSubscription = new CompositeSubscription();
        this.mDataManager = DataManager.getInstance();
    }

    @Override
    public void detachView() {
        this.mMvpView = null;
        this.mCompositeSubscription.unsubscribe();
        this.mCompositeSubscription = null;
        this.mDataManager = null;
    }

    public boolean isViewAttached(){return mMvpView != null;}


    public T getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached(){
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }


    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    private Dialog mProgressDialog;

    public void showProgressDialog(Context context) {
        if (context instanceof Activity) {
            if (((Activity) context).isFinishing()) {
                return;
            }
        }
        mProgressDialog = new Dialog(context, R.style.prompt_progress_dailog);
        mProgressDialog.setContentView(R.layout.global_progress_layout);
        mProgressDialog.setCanceledOnTouchOutside(false);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }


}
