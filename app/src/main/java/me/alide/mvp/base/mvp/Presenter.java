package me.alide.mvp.base.mvp;

/**
 * Created by wutairui on 16/3/16.
 */
public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}
