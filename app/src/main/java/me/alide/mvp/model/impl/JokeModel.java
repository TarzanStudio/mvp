package me.alide.mvp.model.impl;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;

import me.alide.mvp.application.MyApplication;
import me.alide.mvp.bean.Joke;
import me.alide.mvp.core.Biz;
import me.alide.mvp.core.BizApi;
import me.alide.mvp.core.Callback;
import me.alide.mvp.model.IJokeModel;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by wutairui on 16/4/23.
 */
public class JokeModel implements IJokeModel {


    private static final JokeModel mInstance = new JokeModel();

    public static JokeModel getInstance(){ return mInstance;}

    private JokeModel() {}


    @Override
    public Observable<Joke> getJokes(int size, int page) {

        return Observable.create(new Observable.OnSubscribe<Joke>() {
                    @Override
                    public void call(Subscriber<? super Joke> subscriber) {

                        String params = "?size=" + size + "&&page=" + page;

                        try {
                            Biz.getInstance().get(new Callback() {
                                @Override
                                public void onFailure(Request request, IOException e) {
                                    super.onFailure(request, e);
                                    subscriber.onError(e);
                                }

                                @Override
                                public void onResponse(Response response) throws IOException {
                                    super.onResponse(response);
                                    try {
                                       JSONObject jb  = new JSONObject(response.body().string());
                                        if (jb.optString("status").equals("000000")) {
                                            Joke joke = MyApplication.getInstance().gson.fromJson(jb.toString(), Joke.class);
                                            subscriber.onNext(joke);
                                        } else {
                                          subscriber.onError(new Exception(jb.optString("status")));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        subscriber.onError(new Exception("解析数据失败"));
                                    }
                                }
                            }, params , BizApi.GET_JOKE);
                        }catch (Exception e){
                            subscriber.onError(e);
                        }
                    }
                });
    }
}
