package me.alide.mvp.model;


import me.alide.mvp.bean.Joke;
import rx.Observable;

/**
 * Created by wutairui on 16/4/23.
 */
public interface IJokeModel {

    Observable<Joke> getJokes(int size, int page);
}
