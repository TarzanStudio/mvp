package me.alide.mvp.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cundong.recyclerview.EndlessRecyclerOnScrollListener;
import com.cundong.recyclerview.HeaderAndFooterRecyclerViewAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import me.alide.mvp.R;
import me.alide.mvp.adapter.MainAdapter;
import me.alide.mvp.base.BaseSwipeRefreshLayoutActivity;
import me.alide.mvp.bean.JokeEntity;
import me.alide.mvp.presenter.MainPresenter;
import me.alide.mvp.presenter.iview.MainView;
import me.alide.mvp.utils.RecyclerViewStateUtils;
import me.alide.mvp.utils.ToastUtils;
import me.alide.mvp.widget.LoadingFooter;
import butterknife.Bind;

public class MainActivity extends BaseSwipeRefreshLayoutActivity implements MainView {


    private MainPresenter mMainPresenter;
    private final int REQUEST_COUNT = 20;
    @Bind(R.id.activity_main_recycler_view)
    public RecyclerView mRecyclerView;
    public MainAdapter mMainAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter;
    public ArrayList<JokeEntity> jokes = new ArrayList<>();
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        //intent.putExtra("", "");
        context.startActivity(intent);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mToolbar.setTitle("笑话");
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDefaultDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        mMainAdapter = new MainAdapter(this, jokes);
        mHeaderAndFooterRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(mMainAdapter);
        mRecyclerView.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addOnScrollListener(mOnScrollListener);
    }

    @Override
    protected void initListeners() {

    }

    @Override
    protected void initData() {
        mMainPresenter = new MainPresenter(this);
        mMainPresenter.attachView(this);
        getData(true);
    }

    private void getData(boolean refresh){
        if (refresh){
            mMainPresenter.getJokes(REQUEST_COUNT,mMainPresenter.getPage(),  true);
        }else{
            mMainPresenter.setPage(mMainPresenter.getPage()+1);
            mMainPresenter.getJokes(REQUEST_COUNT, mMainPresenter.getPage(), false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.detachView();
    }


    @Override
    public void onFailure(Throwable e) {
        ToastUtils.show(this, e.getMessage(), ToastUtils.LENGTH_SHORT);
    }

    @Override
    public void onGetDataSuccess(ArrayList<JokeEntity> jokes, boolean refresh) {
        if (refresh)
            this.jokes.clear();
        this.jokes.addAll(jokes);
        mMainAdapter.notifyDataSetChanged();
        WeakReference<MainActivity> ref = new WeakReference<>(this);
        RecyclerViewStateUtils.setFooterViewState(ref.get().mRecyclerView, LoadingFooter.State.Normal);
        new Handler().post(() -> {
           mSwipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onSwipeRefresh() {
        getData(true);
    }

    private EndlessRecyclerOnScrollListener mOnScrollListener = new EndlessRecyclerOnScrollListener() {

        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);

            if (jokes.size() < REQUEST_COUNT) return;
            LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
            if (state == LoadingFooter.State.Loading) return;

            RecyclerViewStateUtils.setFooterViewState(MainActivity.this, mRecyclerView, REQUEST_COUNT, LoadingFooter.State.Loading, null);
            mMainPresenter.setPage(mMainPresenter.getPage() + 1);
            getData(false);


        }
    };


}
