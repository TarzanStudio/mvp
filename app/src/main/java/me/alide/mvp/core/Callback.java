package me.alide.mvp.core;


import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/*
* Created by wutairui on 16/4/6.
*/
public abstract class Callback implements com.squareup.okhttp.Callback {

    @Override
    public void onFailure(Request request, IOException e) {

    }

    @Override
    public void onResponse(Response response) throws IOException {

    }
}
