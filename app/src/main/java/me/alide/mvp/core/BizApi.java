package me.alide.mvp.core;

/**
 * Created by wutairui on 16/3/16.
 */
public class BizApi {


    public static final String BASE_URL = "http://api.1-blog.com/biz/bizserver/";

    public static final String SHARE_NAME = "MVP";

    public static final String DATA_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static final int DEFAULT_DATA_SIZE = 10;

    public static final String TAKE_PICTURE_PATH = "mvp";


    public static final String USER = "user";

    public static final String GET_JOKE = "xiaohua/list.do";

}
