package me.alide.mvp.core;

import com.orhanobut.logger.Logger;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import me.alide.mvp.application.MyApplication;


/**
 * Created by wutairui on 16/3/16.
 */
public class Biz {

    private static Biz ourInstance;
    OkHttpClient okHttpClient;

    public static Biz getInstance() {
        if (ourInstance == null) ourInstance = new Biz();
        return ourInstance;
    }

    private Biz() {
        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(7676, TimeUnit.MILLISECONDS);

        if (MyApplication.getInstance().log) {
            okHttpClient.interceptors().add(chain -> {
                Response response = chain.proceed(chain.request());
                String url = chain.request().urlString();
                Logger.d(url);
                return response;
            });
        }

    }

    public void post(Callback callback, FormEncodingBuilder formEncodingBuilder, String action) throws Exception {
        RequestBody requestBody = formEncodingBuilder.build();
        Request request = new Request.Builder()
                .url(BizApi.BASE_URL + action)
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) throws IOException {
                super.onResponse(response);
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Request request, IOException e) {
                super.onFailure(request, e);
                callback.onFailure(request, e);

            }
        });
    }


    public void get(Callback callback, String params, String action)throws Exception {

        Request request = new Request.Builder()
                .url(BizApi.BASE_URL + action + params)
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) throws IOException {
                super.onResponse(response);
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Request request, IOException e) {
                super.onFailure(request, e);
                callback.onFailure(request, e);

            }
        });

    }
}

