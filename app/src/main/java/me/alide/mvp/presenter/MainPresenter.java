package me.alide.mvp.presenter;

import android.content.Context;

import java.util.ArrayList;

import me.alide.mvp.base.mvp.BasePresenter;
import me.alide.mvp.bean.JokeEntity;
import me.alide.mvp.presenter.iview.MainView;
import rx.Subscriber;

/**
 * Created by wutairui on 16/4/23.
 */
public class MainPresenter extends BasePresenter<MainView> {

    private int page;
    public MainPresenter(Context context) {
        super(context);
    }

    public void getJokes(int size, int page , boolean refresh){

        if (refresh) page = 1;
        mCompositeSubscription.add(mDataManager.getJokes(size, page).subscribe(new Subscriber<ArrayList<JokeEntity>>() {
                    @Override
                    public void onCompleted() {
                        if (MainPresenter.this.mCompositeSubscription != null)
                            MainPresenter.this.mCompositeSubscription.remove(this);
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgressDialog();
                        if (MainPresenter.this.getMvpView() != null){
                            MainPresenter.this.getMvpView().onFailure(e);
                        }
                    }

                    @Override
                    public void onNext(ArrayList<JokeEntity> jokes) {
                        hideProgressDialog();
                        if (MainPresenter.this.getMvpView() != null){
                            MainPresenter.this.getMvpView().onGetDataSuccess(jokes, refresh);
                        }

                    }
                }));
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
