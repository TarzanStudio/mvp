package me.alide.mvp.presenter.iview;

import java.util.ArrayList;

import me.alide.mvp.base.mvp.MvpView;
import me.alide.mvp.bean.JokeEntity;

/**
 * Created by wutairui on 16/4/23.
 */
public interface MainView extends MvpView {

    void onGetDataSuccess(ArrayList<JokeEntity> jokes, boolean refresh);
}
