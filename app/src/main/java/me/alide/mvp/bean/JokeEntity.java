package me.alide.mvp.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wutairui on 16/4/23.
 */
public class JokeEntity  implements Serializable{

    @SerializedName("id")
    public int id;
    @SerializedName("xhid")
    public int xhid;
    @SerializedName("author")
    public String author;
    @SerializedName("content")
    public String content;
    @SerializedName("picUrl")
    public String picUrl;
    @SerializedName("status")
    public String status;
}
