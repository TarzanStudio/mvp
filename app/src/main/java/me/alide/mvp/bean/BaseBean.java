package me.alide.mvp.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wutairui on 16/4/23.
 */
public class BaseBean implements Serializable{

    @SerializedName("status")
    public String status;

    @SerializedName("desc")
    public String desc;


}
