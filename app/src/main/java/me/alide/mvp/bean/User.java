package me.alide.mvp.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wutairui on 16/4/23.
 */
public class User implements Serializable {

    @SerializedName("uuid")
    public String uuid;

    @SerializedName("phoneNum")
    public String phoneNum;
}
