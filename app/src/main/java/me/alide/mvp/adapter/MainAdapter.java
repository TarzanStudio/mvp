package me.alide.mvp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import me.alide.mvp.R;
import me.alide.mvp.bean.JokeEntity;

/**
 * Created by wutairui on 16/4/23.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {


    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private ArrayList<JokeEntity> jokes;
    public MainAdapter(Context context, ArrayList<JokeEntity> jokes) {
        mContext = context;
        this.jokes = jokes;
       mLayoutInflater =  LayoutInflater.from(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.joke_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            holder.contentTv.setText(jokes.get(position).content);
        Glide.with(mContext).load(jokes.get(position).picUrl).into(holder.coverIv);
    }

    @Override
    public int getItemCount() {
        if (jokes == null) return 0;
        return jokes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView contentTv;
        ImageView coverIv;

        public ViewHolder(View itemView) {
            super(itemView);
            contentTv  = (TextView) itemView.findViewById(R.id.joke_item_view_content);
            coverIv = (ImageView) itemView.findViewById(R.id.jole_item_view_pic);
        }
    }



}
