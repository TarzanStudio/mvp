package me.alide.mvp.application;

import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;

import com.anupcowkur.reservoir.Reservoir;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.logger.Logger;

import java.util.LinkedList;

import me.alide.mvp.bean.User;
import me.alide.mvp.core.BizApi;
import me.alide.mvp.utils.CommonUtil;
import me.alide.mvp.utils.SharePreUtil;


/**
 * Created by wutairui on 16/3/16.
 */
public class MyApplication extends Application {

    public LinkedList<Activity> activitys = new LinkedList<Activity>();
    private static MyApplication appInstance = new MyApplication();
    public boolean log = true;
    public boolean isLogin = false;
    public User mUser;
    public Gson gson;
    public static final long ONE_KB = 1024L;
    public static final long ONE_MB = ONE_KB * 1024L;
    public static final long CACHE_DATA_MAX_SIZE = ONE_MB * 3L;
    public static MyApplication getInstance() {
        return appInstance;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
        Logger.init();
        this.initGson();
        this.initReservoir();

        SharePreUtil.getInstance().init(this);
        CommonUtil.getInstance().init(this);
        initUser();
        initInsInfo();
    }

    private void initUser(){
        String userStr = SharePreUtil.getInstance().getString(BizApi.USER);
        if (log)
            Logger.d(userStr);
        if (!TextUtils.isEmpty(userStr)) {
            Gson gson = new Gson();
            mUser = gson.fromJson(userStr, User.class);
            isLogin = true;
        }
    }

    public void initInsInfo(){
       //从缓存初始化全局数据
    }
    private void initGson(){
        this.gson = new GsonBuilder()
                .setDateFormat(BizApi.DATA_FORMAT)
                .create();

    }

    //初始化缓存空间
    private void initReservoir(){
        try {
            Reservoir.init(this, CACHE_DATA_MAX_SIZE, this.gson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加activity到LinkedList集合
     *
     * @param activity
     */
    public void addActivity(Activity activity) {
        activitys.add(activity);

    }

    /**
     * 退出集合所有的activity
     */
    public void exit() {
        try {
            for (Activity activity : activitys) {
                if (activity != null) {
                    activity.finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}
