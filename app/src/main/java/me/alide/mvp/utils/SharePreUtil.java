package me.alide.mvp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import me.alide.mvp.core.BizApi;

public class SharePreUtil {
	
	public static final String ADDRESS_UNSERVER = "address_unserver";
    private SharePreUtil() {
    }

    private static SharedPreferences mPres;
    private static SharePreUtil mInstance;

    public void init(Context context) {
        mPres = context.getSharedPreferences(BizApi.SHARE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        Editor edit = mPres.edit();
        edit.clear();
        edit.apply();
    };

    public static SharePreUtil getInstance() {
        if (mInstance == null) {
            mInstance = new SharePreUtil();
        }
        return mInstance;
    }

    public String getString(String key) {
        return mPres.getString(key, "");
    }

    public int getInt(String key) {
        return mPres.getInt(key, -1);
    }

    public void putLong(String key, long value) {
        Editor edit = mPres.edit();
        edit.putLong(key, value);
        edit.apply();
    }

    public long getLong(String key) {
        return mPres.getLong(key, -1);
    }

    public void putString(String key, String value) {
        Editor edit = mPres.edit();
        edit.putString(key, value);
        edit.apply();
    }

    public void putInt(String key, int value) {
        Editor edit = mPres.edit();
        edit.putInt(key, value);
        edit.apply();
    }

    public void putBoolean(String key, boolean value) {
        Editor edit = mPres.edit();
        edit.putBoolean(key, value);
        edit.apply();
    }

    public boolean getBoolean(String key, boolean value) {
        return mPres.getBoolean(key, value);
    }

    public boolean getBoolean(String key) {
        return mPres.getBoolean(key, false);
    }
}