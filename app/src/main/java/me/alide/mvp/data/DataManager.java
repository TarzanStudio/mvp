package me.alide.mvp.data;

import java.util.ArrayList;

import me.alide.mvp.bean.Joke;
import me.alide.mvp.bean.JokeEntity;
import me.alide.mvp.model.impl.JokeModel;
import me.alide.mvp.utils.RxUtils;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by wutairui on 16/4/23.
 */
public class DataManager {

    public static DataManager mDataManager;

    public JokeModel mJokeModel;

    public synchronized static DataManager getInstance() {
        if (mDataManager == null){
            mDataManager = new DataManager();
        }
        return mDataManager;
    }

    private DataManager(){
        mJokeModel = JokeModel.getInstance();
    }


    public Observable<ArrayList<JokeEntity>> getJokes(int size, int page){
        return this.mJokeModel.getJokes(size, page)
                .map(new Func1<Joke, ArrayList<JokeEntity>>() {
                    @Override
                    public ArrayList<JokeEntity> call(Joke joke) {

                        return joke.detail;
                    }
                })
                .compose(RxUtils.applyIOToMainThreadSchedulers());

    }
}
